#!/bin/bash

function printMessage
{
    NOW=$(date +"%Y-%m-%d %T")
    echo "[$NOW] ${1}"
}
function finishError
{
    printMessage "Error: ${1}"
    exit 1
}