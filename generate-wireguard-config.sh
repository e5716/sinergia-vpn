#!/bin/bash

NUMBER_OF_CLIENTS="$1"
REMOTE_IP="$2"
REMOTE_PORT="$3"
INTERFACE_NAME="wgvpn"
OUTPUT_DIR="wg-outputs"

source utils.sh

function validateArgs
{
    if [ -z $NUMBER_OF_CLIENTS ] || [ -z $REMOTE_IP ] || [ -z $REMOTE_PORT ] || [ -z $INTERFACE_NAME ]
    then
        finishError "Required arguments of the script are missing: number of clients to generate, remote IP, remote port or interface name."
    fi

    if [ $NUMBER_OF_CLIENTS -gt 253 ]
    then
        finishError "Cannot assign more than 253 hosts to the VPN network"
    fi
}
function checkDependenciesInstalled
{
    which tee &> /dev/null || finishError "tee is not installed, but is required to generate your keys"
    which wg &> /dev/null || finishError "Wireguard is not installed, but is required to generate your keys and connect to the VPN"
}
function generateKeys
{
    printMessage "Generating Wireguard {public, private, preshared} keys for ${NUMBER_OF_CLIENTS} clients + server"

    cd $OUTPUT_DIR
    for I in $(seq 0 $NUMBER_OF_CLIENTS)
    do
        FILENAME=""
        if [ $I -eq 0 ]
        then
            FILENAME="server"
        else
            FILENAME="client_${I}"
        fi

        umask 077
        wg genkey | tee $FILENAME.key | wg pubkey > $FILENAME.pub
        wg genpsk > $FILENAME.psk
    done

    rm -f server.psk # We don't need this PSK

    cd ..
}
function deleteTemplateComments
{
    FILE="$1"
    sed -i "s/^#.*$//g" $FILE || finishError "Could not delete template comments"
    sed -i '/^$/d' $FILE # Delete empty lines
}
function replaceVariable
{
    ORIGINAL="$1"
    REPLACEMENT=$(echo "$2" | sed -e 's/[\/&]/\\&/g') # Escape keys; from https://stackoverflow.com/a/2705678
    FILE="$3"
    sed -i "s/$ORIGINAL/$REPLACEMENT/g" $FILE || finishError "An error occured when trying to replace $ORIGINAL on the template"
}
function generateConfigClients
{
    printMessage "Generate config files for each client"

    for I in $(seq 1 $NUMBER_OF_CLIENTS)
    do
        cd $OUTPUT_DIR

        PREFIX="client_$I"
        CURRENT_CONFIG_FILE="${PREFIX}.conf"

        cp -f ../templates/wireguard-client.conf $CURRENT_CONFIG_FILE || finishError "Could not copy the template config for the client"

        deleteTemplateComments $CURRENT_CONFIG_FILE
        replaceVariable "{{{CLIENT_COUNTER}}}" "$I" $CURRENT_CONFIG_FILE
        replaceVariable "{{{CLIENT_PRIVKEY}}}" $(cat ${PREFIX}.key) $CURRENT_CONFIG_FILE
        replaceVariable "{{{SERVER_PUBKEY}}}" $(cat server.pub) $CURRENT_CONFIG_FILE
        replaceVariable "{{{CLIENT_PSK}}}" $(cat ${PREFIX}.psk) $CURRENT_CONFIG_FILE
        replaceVariable "{{{VPS_IP}}}" $REMOTE_IP $CURRENT_CONFIG_FILE
        replaceVariable "{{{VPS_PORT}}}" $REMOTE_PORT $CURRENT_CONFIG_FILE
        cd ..
    done
}
function generateConfigServer
{
    printMessage "Generate config file of the server"

    cd $OUTPUT_DIR

    cp -f ../templates/wireguard-server.conf $INTERFACE_NAME.conf || finishError "Could not copy the template config for the server"
    deleteTemplateComments $INTERFACE_NAME.conf

    replaceVariable "{{{VPS_PORT}}}" $REMOTE_PORT $INTERFACE_NAME.conf
    replaceVariable "{{{SERVER_PRIVKEY}}}" $(cat server.key) $INTERFACE_NAME.conf

    for I in $(seq 1 $NUMBER_OF_CLIENTS)
    do
        # The netmask /32 tells Wireguard there will be only one host connected using these peer values
        cat << EOF >> $INTERFACE_NAME.conf
# Peer: client_${I}
[Peer]
PublicKey = $(cat client_${I}.pub)
PresharedKey = $(cat client_${I}.psk)
AllowedIPs = 192.168.69.${I}/32
PersistentKeepalive = 60
EOF
    done

    cd ..
}
function deleteKeys
{
    printMessage "Doing some key cleanup"

    cd $OUTPUT_DIR
    rm -f *.key
    rm -f *.pub
    rm -f *.psk
    cd ..
}
function init
{
    validateArgs
    checkDependenciesInstalled
    generateKeys
    generateConfigClients
    generateConfigServer
    deleteKeys
}

init