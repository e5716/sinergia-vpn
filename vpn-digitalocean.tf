# Terraform config file for DigitalOcean resources
# Author: @Fremontt

# Variables must be declared here before assigning a value on "terraform.tfvars"
variable "accountToken" {}
variable "sshFingerprint" {}
variable "dropletName" {}
variable "dropletRegion" {}
variable "dropletSize" {}
variable "image" {}
variable "firewallName" {}

# Set up the provider: you only need an access token generated from your account
provider "digitalocean" {
  token = var.accountToken
}

# Resources for VPN architecture
resource "digitalocean_droplet" "tf_droplet_vpn" {
  name = var.dropletName
  region = var.dropletRegion
  size = var.dropletSize
  image =  var.image
  backups = false
  monitoring = true
  ipv6 = false
  private_networking = false
  ssh_keys = [var.sshFingerprint]
  resize_disk = false
  tags = ["vpn"]
}
resource "digitalocean_firewall" "tf_firewall_vpn" {
    name = var.firewallName
    droplet_ids = [digitalocean_droplet.tf_droplet_vpn.id]
    tags = ["vpn"]

    # SSH - first time
    inbound_rule {
        protocol = "tcp"
        port_range = "22"
        source_addresses = ["0.0.0.0/0", "::/0"]
    }
    # SSH - Manually Defined on sshd_config; changing this value will break some parts of the script
    inbound_rule {
        protocol = "tcp"
        port_range = "39780"
        source_addresses = ["0.0.0.0/0", "::/0"]
    }
    # VPN
    inbound_rule {
        protocol = "udp"
        port_range = "47469"
        source_addresses = ["0.0.0.0/0", "::/0"]
    }

    outbound_rule {
        protocol = "tcp"
        port_range = "1-65535"
        destination_addresses = ["0.0.0.0/0", "::/0"]
    }
    outbound_rule {
        protocol = "udp"
        port_range = "1-65535"
        destination_addresses = ["0.0.0.0/0", "::/0"]
    }
    outbound_rule {
        protocol = "icmp"
        destination_addresses = ["0.0.0.0/0", "::/0"]
    }
}

# Output variables
output "droplet_ip" {
  value = digitalocean_droplet.tf_droplet_vpn.ipv4_address
}