#!/bin/bash

# I won't use the SB's hosts generator tool because it does not parse well the arguments on the CLI. Also, its social extension is pretty exagerated...how come is going to block Reddit and Whatsapp media?

SOURCE_LIST="https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"
TMP_FILE="/tmp/blacklist.txt"
TMP_CLN_FILE="/tmp/blacklist2.txt"
OUTPUT_FILE="/etc/unbound/community.list.conf"

# Duplicated functions because I don't source another scripts on the remote host
function printMessage
{
    NOW=$(date +"%Y-%m-%d %T")
    echo "[$NOW] ${1}"
}
function finishError
{
    printMessage "Error: ${1}"
    exit 1
}

function fetchList
{
    curl -s -o $TMP_FILE $SOURCE_LIST || finishError "Could not fetch the remote community list"
}
function filterInputs
{
    grep "^0\.0\.0\.0" $TMP_FILE | cut -f 2 -d " " > $TMP_CLN_FILE

    echo "" > $OUTPUT_FILE

    for DOMAIN in $(cat $TMP_CLN_FILE)
    do
        echo -e "local-zone: \"$DOMAIN\" always_nxdomain" >> $OUTPUT_FILE
    done

    sed -i '/^.*0\.0\.0\.0.*$/d' $OUTPUT_FILE
}
function cleanup
{
    rm -f $TMP_FILE $TMP_CLN_FILE
}
function init
{
    fetchList
    filterInputs
    cleanup
    exit
}
init