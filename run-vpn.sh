#!/bin/bash

# By @fremontt

source utils.sh

DO_REGION="sfo3"
DO_DROPLET_NAME="Sinergia-VPN"
DO_DROPLET_SIZE="s-1vcpu-1gb"
DO_FIREWALL_NAME="Sinergia-VPN-Firewall"
HOSTS_TO_CREATE=3 # You can change it with no problem
DEFAULT_PORT=47469 # You need to change this value on Terraform config too (cloud firewall)

# ==================================================== #
#       Logic section: don't touch anything else
# ==================================================== #
ACTION="${1}"
DO_TOKEN="${2}"
DO_SSH_FINGERPRINT="${3}"
TF_EXECPLAN_FOLDER="tf-execution-plan"
DO_IMAGE="fedora-32-x64"
OUTPUT_DIR="wg-outputs"
VPS_IP=""

function checkOutputFolder
{
    if ! [ -d $OUTPUT_DIR ]
    then
        mkdir $OUTPUT_DIR
    fi

    if [ "$(ls -A $OUTPUT_DIR)" ]
    then
        finishError "The ./${OUTPUT_DIR} is not empty; please backup (if needed) and delete everything inside to generate new keys and config files"
    fi
}
function checkArgs
{
    if [ -z $ACTION ] || [ -z $DO_TOKEN ] || [ -z $DO_SSH_FINGERPRINT ]
    then
        finishError "Required args are not defined"
    fi
}
function createVPS
{
    printMessage "Creating resources on your DigitalOcean account"
    terraform init || finishError "Could not start Terraform"
    terraform plan -var="accountToken=${DO_TOKEN}" -var="sshFingerprint=${DO_SSH_FINGERPRINT}" -var="dropletName=${DO_DROPLET_NAME}" -var="dropletRegion=${DO_REGION}" -var="dropletSize=${DO_DROPLET_SIZE}" -var="image=${DO_IMAGE}" -var="firewallName=${DO_FIREWALL_NAME}" -out "${TF_EXECPLAN_FOLDER}" || finishError "Could not create a Terraform plan"
    terraform apply "${TF_EXECPLAN_FOLDER}" || finishError "Could not apply changes into your DigitalOcean account"
}
function destroyVPS
{
    terraform plan -destroy -var="accountToken=${DO_TOKEN}" -var="sshFingerprint=${DO_SSH_FINGERPRINT}" -var="dropletName=${DO_DROPLET_NAME}" -var="dropletRegion=${DO_REGION}" -var="dropletSize=${DO_DROPLET_SIZE}" -var="image=${DO_IMAGE}" -var="firewallName=${DO_FIREWALL_NAME}" -out "${TF_EXECPLAN_FOLDER}" || finishError "Could not update the Terraform plan with your current resources"
    terraform destroy -auto-approve -var="accountToken=${DO_TOKEN}" -var="sshFingerprint=${DO_SSH_FINGERPRINT}" -var="dropletName=${DO_DROPLET_NAME}" -var="dropletRegion=${DO_REGION}" -var="dropletSize=${DO_DROPLET_SIZE}" -var="image=${DO_IMAGE}" -var="firewallName=${DO_FIREWALL_NAME}" || finishError "An error has occured when trying to delete your cloud resources"
}
function getIP
{
    VPS_IP=$(terraform output droplet_ip)
}
function generateConfig
{
    bash ./generate-wireguard-config.sh $HOSTS_TO_CREATE $1 $DEFAULT_PORT || finishError "Could not generate Wireguard configs"
}
function generateInventory
{
    echo -e "[vps]\nvpn_server ansible_host=${1} ansible_connection=ssh ansible_ssh_port=22 ansible_ssh_common_args='-o StrictHostKeyChecking=no' ansible_connect_timeout=120 ansible_python_interpreter=/usr/bin/python3\n" > inventory.ini # The first time, the default SSH port is 22
}
function runPlaybook
{
    ansible-playbook -i inventory.ini vps_playbook.yaml || finishError "Error on Ansible playbook execution"
}
function init
{
    checkArgs

    case $ACTION in
        "create")
            checkOutputFolder
            createVPS
            getIP
            generateConfig $VPS_IP
            generateInventory $VPS_IP
            runPlaybook
            printMessage "Please do not delete ${TF_EXECPLAN_FOLDER} folder, otherwise you could have issues when deleting the resources at DigitalOcean"
            printMessage "Deploy finished, wait a couple of minutes to let your server restart and then connect to your VPN"
            ;;
        "destroy")
            destroyVPS
            ;;
        *)
            finishError "No action specified. Please run '$0 <action: create|destroy> <do_token> <do_ssh_fingerprint>'"
            ;;
    esac
    exit
}

init