# Sinergia VPN

My personal VPN running on a cloud instance, focused on privacy. Based on Wireguard, Unbound and [StevenBlack hosts](https://github.com/StevenBlack/hosts).

[[_TOC_]]

## Getting started

This script needs Terraform and Ansible installed on your computer. Also you need a DigitalOcean account to deploy your VPN server there.

Please generate a DigitalOcean Personal Access Token and associate a SSH key to your account.

**If you need free credits, [use my DO referral link to get $100 USD](https://m.do.co/c/397805aa0e0e)**.

### Deploy

The deployment of the VPN server is pretty easy, just run `./run-vpn.sh create <your_do_token> <your_ssh_key_fingerprint>`. Don't forget to backup the `./wg-outputs/` folder once finished, as this is the location where your generated keys are available to connect the VPN.

You can destroy your instance anytime, running `./run-vpn.sh destroy <your_do_token> <your_ssh_key_fingerprint>`.

Please take in account each time you run a new VPN creation, new keys are generated. So you will need to delete the `./wg-outputs/` directory before running a new VPN deploy.

#### Examples

Please take a look to the [**Examples** section of LOS-Infra](https://gitlab.com/fremontt/minimal-docker-for-lineageos/infra#examples); the execution is exactly the same.

### Connecting to the VPN

By default, this tool will generate 3 configuration files for your devices, in the directory `./wg-outputs/client_<NUMBER>.conf`

> (Optional) You can change the value of the variable `HOSTS_TO_CREATE` in `./run-vpn.sh` if you want a different number of device config files to be generated.

#### On Linux

I suggest use NetworkManager, as it has native support for Wireguard.

Assuming this is your `./wg-outputs/client_1.conf`

```
[Interface]
Address = 192.168.69.1/24
PrivateKey = 0000000000000000000000000=
DNS = 192.168.69.254
[Peer]
PublicKey = 111111111111111111111111111111111111111=
PresharedKey = 2222222222222222222222222222222222222=
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = 100.101.102.103:47469
PersistentKeepalive = 60
```

1. On NetworkManager, add a new connection of type **VPN > Wireguard**.

![Create a new Wireguard VPN on NetworkManager](static/0.png)

2. Assign a name (no spaces, no symbols, just alphanumeric and less than 15 characters).

3. Under **Wireguard Interface** tab, fill the *Private key* and check the *Autoroute peers* fields.

![Setup private key on Wireguard NetworkManager](static/1.png)

4. Add a new peer using the button **Peers**.

5. Fill the *Public key*, *Allowed IPs*, *Endpoint address*, *Endpoint port*, *Preshared key* and *Persistent keepalive* fields.

![Setup peer on Wireguard NetworkManager](static/2.png)

6. Under **IPv4 tab** tab, choose the *Method: Manual*, fill the *DNS Server* with the `DNS` config value and add a new IP using the value in `Address` config (no need to put the `/24`); NetworkManager should fill automatically the *Netmask*.

![Setup IPv4 on Wireguard NetworkManager](static/3.png)

7. Under **IPv4 tab** tab, choose the *Method: Ignored*.

![Setup IPv6 on Wireguard NetworkManager](static/4.png)

8. Save your changes and connect to your VPN.

#### On Android

1. On your phone device, download [Wireguard app](https://f-droid.org/en/packages/com.wireguard.android/)
2. On your computer, generate a QR code of your client config running `qrencode -t ansiutf8 -r ./wg-outputs/client_<NUMBER>.conf`.
3. Open the Wireguard app and add a new tunnel using "Scan from QR code"

## License

This is an open-source project by **@fremontt** under license MIT